package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ServletSalida
 */
//@WebServlet("/ServletSalida")
@WebServlet(
        name = "ServletSalida",
        urlPatterns = {"/ServletSalida", "/salida"}
)

public class ServletSalida extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletSalida() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
		response.getWriter().append("<h1> ServletSalida</h1>");
		if (request.getAttribute("desde") == null ) {
			response.getWriter().append("Acceso directo, sin forward");
		} else {
			response.getWriter().append("Desde " + request.getAttribute("desde").toString());
		}
	}

}
