package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Entrada4
 */
@WebServlet(
		name = "ServletEntrada4", 
		urlPatterns = { 
				"/ServletEntrada4", 
				"/entrada4"
		})
public class ServletEntrada4 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletEntrada4() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("desde", "Entrada 4: usando include en vez de forward");
		ServletContext context = request.getServletContext();
		RequestDispatcher dispatcher = context.getNamedDispatcher("ServletSalida");
		
        response.setContentType("text/html;charset=UTF-8");
		response.getWriter().append("Estoy en Entrada 4, al usar include esto no se pierde<br>");
		dispatcher.include(request, response);
		System.out.println("Ojo que vuelve la petiición aqui!!");
		response.getWriter().append("<h2>Ojo que vuelve la petición aqui!!</h2>");
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
