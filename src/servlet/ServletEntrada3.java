package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ServletEntrada3
 */
@WebServlet({ "/ServletEntrada3", "/entrada3" })
public class ServletEntrada3 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletEntrada3() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("desde", "Entrada 3: context.getNamedDispatcher. Nombre de la clase");
		ServletContext context = request.getServletContext();
		RequestDispatcher dispatcher = context.getNamedDispatcher("ServletSalida");
		
		response.getWriter().append("Estoy en Entrada 3, al usar forward esto se va a perder<br>");
		dispatcher.forward(request, response);
		System.out.println("Ojo que vuelve la petiición aqui!!");
		response.getWriter().append("<h2>Ojo que vuelve la petición aqui!!</h2>");
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
