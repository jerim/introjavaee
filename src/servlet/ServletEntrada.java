package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ServletEntrada
 */
@WebServlet({"/ServletEntrada", "/entrada"})
public class ServletEntrada extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletEntrada() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("desde", "Entrada 1: request.getRequestDispatcher(\"/salida\"). Ruta absoluta o relativa");
	    RequestDispatcher dispatcher = request.getRequestDispatcher("salida");
	      
	    dispatcher.forward(request, response);	
		System.out.println("Ojo que vuelve la petiición aqui!!");
		response.getWriter().append("<h2>Ojo que vuelve la petición aqui!!</h2>");
		return;
	}

}
